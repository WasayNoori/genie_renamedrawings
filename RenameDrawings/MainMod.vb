﻿Imports EdmLib
Imports System.Windows.Forms

Module MainMod
    Sub main()
        Dim vault As New EdmVault5
        vault.LoginAuto("Appdemo", 0)

        Dim file As IEdmFile5
        Dim fold As IEdmFolder5
        StaticSettings.vault = vault

        file = vault.GetFileFromPath("D:\Appdemo\SinglePart.slddrw", fold)

        Dim refpart As String
        refpart = Refs.GetReferencedDoc(file, fold)
        Dim drawingname As String
        drawingname = IO.Path.GetFileNameWithoutExtension(file.Name)
        If String.IsNullOrEmpty(refpart) OrElse String.IsNullOrEmpty(DrawingName) Then
            Logger.LogError(file.Name & vbTab & "Drawing name or referenced name was null. ")
            Exit Sub
        End If




        If DrawingName.ToLower = refpart.ToLower Then

            Logger.LogThis("Drawing already named as referenced document. ", file.Name)
            Exit Sub
        End If



        If FileCheck.Exists(DrawingName & ".slddrw") Then
            Logger.LogThis(DrawingName & ".slddrw already exists. ", file.Name)
            Exit Sub
        End If

        Try

            file.RenameEx(0, refpart & ".slddrw", 0)
            Logger.LogThis("File renamed to " & refpart, file.Name)
        Catch ex As Exception
            Logger.LogError(file.Name & vbTab & "Error in renaming. ", ex)
        End Try

    End Sub
End Module
