﻿Imports EdmLib

Public Class Refs
    Public Shared Function GetReferencedDoc(file As IEdmFile5, fold As IEdmFolder7) As String

        If file Is Nothing OrElse fold Is Nothing Then
            Return String.Empty
        End If
        Try

            Dim enumRef As IEdmReference9
            Dim top As Boolean = False
            Dim projName As String

            enumRef = file.GetReferenceTree(fold.ID)
            Dim pos As IEdmPos5
            pos = enumRef.GetFirstChildPosition(projName, top, True)
            Dim refPath As String = ""
            Dim ref As IEdmReference9
            While Not pos.IsNull
                ref = enumRef.GetNextChild(pos)
                refPath = ref.Name
                Return refPath
            End While

            Return refPath
        Catch ex As Exception
            Return String.Empty
        End Try



    End Function

End Class
