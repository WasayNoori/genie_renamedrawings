﻿Imports EdmLib

Public Class FileCheck
    Public Shared Function Exists(filename As String) As Boolean
        Dim search As IEdmSearch7
        If StaticSettings.vault Is Nothing Then
            'log
            Return False
        End If


        search = StaticSettings.vault.CreateSearch()
        search.SetToken(EdmSearchToken.Edmstok_FindFiles, True)
        search.SetToken(EdmSearchToken.Edmstok_Name, filename)
        Dim searchres As IEdmSearchResult5

        searchres = search.GetFirstResult()
        If searchres Is Nothing Then
            Return False
        Else
            While Not searchres Is Nothing
                If searchres.Name.ToLower = filename.ToLower Then
                    Return True
                End If
                searchres = search.GetNextResult()
            End While
            Return False



        End If
    End Function
End Class
