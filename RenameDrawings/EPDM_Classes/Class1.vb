﻿Imports EdmLib

Public Class Class1
    Implements IEdmAddIn5
    Dim vault As EdmVault5
    Dim PathLocation As String
    Dim currentSetupPage As SetupPage



    Public Sub GetAddInInfo(ByRef poInfo As EdmAddInInfo, poVault As IEdmVault5, poCmdMgr As IEdmCmdMgr5) Implements IEdmAddIn5.GetAddInInfo
        On Error GoTo ErrHand
        ' Fill in the AddIn's description
        poInfo.mbsAddInName = "Rename Drawings Task"
        poInfo.mbsCompany = "Hawk Ridge Systems"
        poInfo.mbsDescription = "Automatically renames drawings to match their referenced documents."
        poInfo.mlAddInVersion = 1

        ' Minimum SolidWorks Enterprise PDM version needed for VB .Net Add-Ins is 2010
        poInfo.mlRequiredVersionMajor = 10
        poInfo.mlRequiredVersionMinor = 0

        'Register this add-in as a task add-in
        poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskRun)
        'Register this add-in as being able to append its own property pages in the administration tool
        poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskSetup)
        'Say that we want to be called when the task is launched on the client computer
        poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskLaunch)
        'Say that we provide extra details in the details dialog box in the task list in the administration tool
        poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskDetails)
        'Say that we want to be called when the launch dialog box gets closed
        poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskLaunchButton)
        'Say that we want to be called when the setup wizard is closed
        poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskSetupButton)

        Exit Sub

ErrHand:
        Dim v11 As IEdmVault11
        v11 = poVault
        MsgBox(v11.GetErrorMessage(Err.Number))

    End Sub

    Public Sub OnCmd(ByRef poCmd As EdmCmd, ByRef ppoData As Array) Implements IEdmAddIn5.OnCmd
        On Error GoTo ErrHand

        Dim vault As IEdmVault12
        vault = poCmd.mpoVault
        StaticSettings.vault = vault

        Select Case poCmd.meCmdType
            Case EdmCmdType.EdmCmd_TaskDetails
                OnTaskDetails(poCmd, ppoData)
            Case EdmCmdType.EdmCmd_TaskLaunch
                OnTaskLaunch(poCmd, ppoData)
            Case EdmCmdType.EdmCmd_TaskLaunchButton
                OnTaskLaunchButton(poCmd, ppoData)
            Case EdmCmdType.EdmCmd_TaskRun
                OnTaskRun(poCmd, ppoData)
            Case EdmCmdType.EdmCmd_TaskSetup
                OnTaskSetup(poCmd, ppoData)
            Case EdmCmdType.EdmCmd_TaskSetupButton
                OnTaskSetupButton(poCmd, ppoData)
        End Select
        Exit Sub

ErrHand:
        Dim v11 As IEdmVault11
        v11 = poCmd.mpoVault
        MsgBox(v11.GetErrorMessage(Err.Number))
    End Sub


    Private Sub OnTaskLaunch(ByRef poCmd As EdmCmd, ByRef ppoData As System.Array)
        On Error Resume Next

        'Display a UI just because we can. Typically you would want to display a message box
        'Where the user enteres data that is passed to the task add-in via the props pointer.
        Dim v11 As IEdmVault11
        v11 = poCmd.mpoVault
       v11.MsgBox(poCmd.mlParentWnd, "Task submitted. It may take a few minutes to rename the file. " & vbCrLf & "Do not resubmit. ") 


      

    End Sub

    Private Sub OnTaskRun(ByRef poCmd As EdmCmd, ByRef ppoData As System.Array)
        vault = New EdmVault5
        vault = poCmd.mpoVault
        StaticSettings.vault = vault


        'Get the property interface used to access the framework
        Dim inst As IEdmTaskInstance
        Dim objType As Long
        Dim fileList As New List(Of String)
        inst = poCmd.mpoExtra
       
        Dim FilePath As String
        Dim fileID, foldid As Long

        Dim file As IEdmFile8
        Dim varValue As String
        Dim newFileName As String
        Dim logLocation As String
        logLocation = inst.GetValEx("logpath")
        StaticSettings.LogPath = logLocation




        On Error GoTo ErrHand
        'Inform the framework that we have started
        inst.SetStatus(EdmTaskStatus.EdmTaskStat_Running)

        'Format a message that will be displayed in the task list
        Dim msg As String


        'This is the main worker loop that does all of the important work. 
        'In our case it just beeps and sleeps
        Dim idx As Integer
        idx = LBound(ppoData)
        Dim maxPos As Integer
        maxPos = UBound(ppoData)
        inst.SetProgressRange(maxPos, 0, msg + CStr(idx))

        Logger.LogError("Date =" & Date.Today.ToShortDateString & " " & Date.Today.ToShortTimeString)
        Logger.LogError("")
        Logger.LogThis("Date =" & Date.Today.ToShortDateString & " " & Date.Today.ToShortTimeString)
        Logger.LogThis("")
        While idx <= maxPos




            FilePath = ppoData(idx).mbsStrData1
            fileID = ppoData(idx).mlObjectID1
            foldid = ppoData(idx).mlObjectID2
            'Make sure there is a file here. "
            Processfile(fileID, foldid)


            inst.SetProgressPos(idx, msg + CStr(idx))
            idx = idx + 1




            'Handle the cancel button in the task list
            If inst.GetStatus() = EdmTaskStatus.EdmTaskStat_CancelPending Then
                inst.SetStatus(EdmTaskStatus.EdmTaskStat_DoneCancelled)
                Exit Sub
            End If

            'Handle temporary suspension of the task
            If inst.GetStatus() = EdmTaskStatus.EdmTaskStat_SuspensionPending Then
                inst.SetStatus(EdmTaskStatus.EdmTaskStat_Suspended)
                While inst.GetStatus() = EdmTaskStatus.EdmTaskStat_Suspended
                    System.Threading.Thread.Sleep(1000)
                End While
                If inst.GetStatus() = EdmTaskStatus.EdmTaskStat_ResumePending Then
                    inst.SetStatus(EdmTaskStatus.EdmTaskStat_Running)
                End If
            End If
        End While



        inst.SetStatus(EdmTaskStatus.EdmTaskStat_DoneOK)
        Exit Sub

ErrHand:
        'Return errors to the framework by failing the task
        Logger.LogThis("Error in executing task. " & Err.Description)
        inst.SetStatus(EdmTaskStatus.EdmTaskStat_DoneFailed, Err.Number, "The test task failed!")
    End Sub



    Private Sub processfile(fileid As Long, foldid As Long)
        If fileid = 0 Or foldid = 0 Then
            Logger.LogError("File ID was zero")
            Exit Sub
        End If
        Dim file As IEdmFile8
        file = StaticSettings.vault.GetObject(EdmObjectType.EdmObject_File, fileid)
        Dim fold As IEdmFolder7
        fold = StaticSettings.vault.GetObject(EdmObjectType.EdmObject_Folder, foldid)

        If file Is Nothing OrElse fold Is Nothing Then
            Logger.LogError("File or Folder was nothing. File ID =" & fileid)
            Exit Sub
        End If

        If file.Name.ToString.ToLower.EndsWith(".slddrw") Then
            Dim oldname As String = file.Name


            'make sure file is checked in. 
            If file.IsLocked Then
                Logger.LogError(file.Name & vbTab & "File is checked out.")
                Exit Sub
            End If

            Dim refpart As String
            refpart = Refs.GetReferencedDoc(file, fold)
            If String.IsNullOrEmpty(refpart) Then
                Logger.LogError(file.Name & vbTab & "No references found. ")
                Exit Sub
            End If



            Dim DrawingName As String
            DrawingName = IO.Path.GetFileNameWithoutExtension(file.Name)
            refpart = IO.Path.GetFileNameWithoutExtension(refpart)
            If String.IsNullOrEmpty(refpart) OrElse String.IsNullOrEmpty(DrawingName) Then
                Logger.LogError(file.Name & vbTab & "Drawing name or referenced name was null. ")
                Exit Sub
            End If




            If DrawingName.ToLower = refpart.ToLower Then

                Logger.LogThis("Drawing already named as referenced document. ", file.Name)
                Exit Sub
            End If



            If FileCheck.Exists(refpart & ".slddrw") Then
                Logger.LogThis(DrawingName & ".slddrw already exists. ", file.Name)
                Exit Sub
            End If

            Try

                file.RenameEx(0, refpart & ".slddrw", 0)
                Logger.LogThis("File renamed to " & refpart, oldname)
            Catch ex As Exception
                Logger.LogError(file.Name & vbTab & "Error in renaming. ", ex)
            End Try
       

        End If
    End Sub
    Private Sub OnTaskSetup(ByRef poCmd As EdmCmd, ByRef ppoData As System.Array)

        Try
            'Get the property interface used to access the framework
            Dim props As IEdmTaskProperties
            props = poCmd.mpoExtra

            'Turn on some properties. In this case that we can be launched from change state, 
            'that we extend the details page, that we want to be called when the task is
            'launched, and that we support scheduling
            props.TaskFlags = EdmTaskFlag.EdmTask_SupportsChangeState + EdmTaskFlag.EdmTask_SupportsDetails + EdmTaskFlag.EdmTask_SupportsInitExec + EdmTaskFlag.EdmTask_SupportsScheduling

            'Set menu commands that will launch this task from Windows Explorer
            Dim cmds(0) As EdmTaskMenuCmd
            cmds(0).mbsMenuString = "Rename Drawings"
            cmds(0).mbsStatusBarHelp = "Rename Drawings to Referenced Documents."
            cmds(0).mlCmdID = 1
            cmds(0).mlEdmMenuFlags = EdmMenuFlags.EdmMenu_Nothing
            props.SetMenuCmds(cmds)

            'Add a custom setup page. The class SetupPage is a user control with an
            'editbox in. The loadData function populates the editbox from a
            'variable in the props interface. The saving of properties is handled
            'in the method OnTaskSetupButton 
            currentSetupPage = New SetupPage
            currentSetupPage.CreateControl()
            'currentSetupPage.LoadData(poCmd)

            Dim pages(0) As EdmTaskSetupPage
            pages(0).mbsPageName = "Setup Page"
            pages(0).mlPageHwnd = currentSetupPage.Handle.ToInt32
            pages(0).mpoPageImpl = currentSetupPage

            props.SetSetupPages(pages)
        Catch ex As Exception
            '  MessageBox.Show("Error in Setup. " & ex.Message)
        End Try

    End Sub
    Private Sub OnTaskDetails(ByRef poCmd As EdmCmd, ByRef ppoData As System.Array)
        Try
            'Dim inst As IEdmTaskInstance
            'inst = poCmd.mpoExtra

            ''Create a custom page for the tast in the task properties dialog box 
            ''brought up from the task list. The TaskDetailsPage class is a 
            ''user control. The LoadData member will fill in some editboxes with
            ''values from the inst pointer's GetValEx method.
            'Dim myPage As TaskDetailsPage
            'myPage = New TaskDetailsPage
            'myPage.CreateControl()
            'poCmd.mpoExtra = myPage

            'poCmd.mlParentWnd = myPage.Handle.ToInt32
            'poCmd.mbsComment = "My Test Page"

            'myPage.LoadData(inst)
        Catch ex As Exception
            ' MessageBox.Show("Error in TaskDetail sub " & ex.Message)
        End Try


    End Sub
    Private Sub OnTaskLaunchButton(ByRef poCmd As EdmCmd, ByRef ppoData As System.Array)

        'This callback is called when the user pressed OK or Cancel in the launch dialog box
        'We don't need it since we don't display a card.

    End Sub
    Private Sub OnTaskSetupButton(ByRef poCmd As EdmCmd, ByRef ppoData As System.Array)
        'The custom setup page in currentSetupPage was created in method OnTaskSetup. The 
        'StoreData method will save the content of the editbox in the user control to
        'the IEdmTaskProperties interface in poCmd.mpoExtra.
        If poCmd.mbsComment = "OK" And Not currentSetupPage Is Nothing Then
            currentSetupPage.StoreData(poCmd)
        End If
        currentSetupPage = Nothing
    End Sub
End Class
