﻿Imports EdmLib
Imports System.Windows.Forms

Public Class SetupPage
    Dim vault As EdmVault5

    Private Sub SetupPage_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub


    Public Sub LoadData(ByRef poCmd As EdmCmd)
        vault = New EdmVault5
        vault = poCmd.mpoVault

        'Get the property interface used to access the framework
        Dim props As IEdmTaskProperties
        props = poCmd.mpoExtra

        tbLogPath.Text = props.GetValEx("logpath")
        StaticSettings.LogPath = tbLogPath.Text




    End Sub

    Public Sub StoreData(ByRef poCmd As EdmCmd)

        'Get the property interface used to access the framework
        Dim props As IEdmTaskProperties
        props = poCmd.mpoExtra

        'Make sure the user has entered a value in our editbox


        'Save the value entered by the user
        props.SetValEx("logpath", tbLogPath.Text)
    

    End Sub

    

    Private Sub BrowseBtn_Click(sender As Object, e As EventArgs) Handles BrowseBtn.Click
        Dim dialres As DialogResult
        dialres = FBD.ShowDialog
        If dialres = DialogResult.OK Then
            tbLogPath.Text = FBD.SelectedPath

        End If
    End Sub
End Class
