﻿Public Class Logger
    Public Shared Sub LogThis(msg As String, Optional filename As String = "")
        On Error Resume Next

        If String.IsNullOrEmpty(StaticSettings.LogPath) Then
            Exit Sub
        End If

        If IO.Directory.Exists(StaticSettings.LogPath) = False Then
            Exit Sub
        End If

        Dim stWriter As New IO.StreamWriter(IO.Path.Combine(StaticSettings.LogPath, "RenameDrawingTaskLog.txt"), True)

        stWriter.WriteLine(filename & vbTab & msg)
        stWriter.Close()
        stWriter.Dispose()

    End Sub

    Public Shared Sub LogError(msg As String, Optional ex As Exception = Nothing)
        On Error Resume Next

        If String.IsNullOrEmpty(StaticSettings.LogPath) Then
            Exit Sub
        End If

        If IO.Directory.Exists(StaticSettings.LogPath) = False Then
            Exit Sub
        End If

        Dim stWriter As New IO.StreamWriter(IO.Path.Combine(StaticSettings.LogPath, "RenameDrawingTaskErrors.txt"), True)

        If Not ex Is Nothing Then
            stWriter.WriteLine(msg & vbTab & ex.Message)
        Else
            stWriter.WriteLine(msg)
        End If
        stWriter.Close()
        stWriter.Dispose()

    End Sub
End Class
